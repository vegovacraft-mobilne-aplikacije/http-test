import 'package:flutter/material.dart';
import 'package:http_test/screens/lists.screen.dart';
import 'package:http_test/screens/register.screen.dart';
import 'package:http_test/screens/start.screen.dart';

import 'screens/home.screen.dart';
import 'screens/list.screen.dart';
import 'screens/login.screen.dart';
import 'screens/user.screen.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const HomeScreen(),
        '/start': (context) => const StartScreen(),
        '/user': (context) => const UserScreen(),
        '/list': (context) => const ListScreen(),
        '/lists': (context) => const ListsScreen(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const RegisterScreen(),
      },
      darkTheme: ThemeData.dark().copyWith(
        colorScheme: const ColorScheme.dark().copyWith(
          primary: Colors.lightGreen,
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              width: 2.0,
            ),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(
                horizontal: 32.0,
                vertical: 16.0,
              ),
            ),
            backgroundColor: MaterialStateProperty.all(Colors.pinkAccent),
          ),
        ),
      ),
      initialRoute: '/',
    );
  }
}
