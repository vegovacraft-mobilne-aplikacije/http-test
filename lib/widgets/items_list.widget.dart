import 'package:flutter/material.dart';
import 'package:http_test/api/client.dart';

class ItemListWidget extends StatefulWidget {
  const ItemListWidget({Key? key}) : super(key: key);

  @override
  State<ItemListWidget> createState() => _ItemListWidgetState();
}

class _ItemListWidgetState extends State<ItemListWidget> {
  List _items = [];

  @override
  void initState() {
    _getItemList();
    super.initState();
  }

  void _getItemList() async {
    final data = await getList(10);

    final list = List.from(data["data"]["attributes"]["items"]);

    setState(() => {
          _items = list,
        });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _items.length,
      itemBuilder: (context, i) {
        final item = _items[i];
        return ListTile(
          title: Text(item["name"]),
          subtitle: Text(item["quantity"].toString()),
        );
      },
    );
  }
}
