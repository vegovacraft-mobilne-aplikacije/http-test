import 'package:flutter/material.dart';
import 'package:http_test/api/client.dart';
import 'package:http_test/models/list.model.dart';

class ListsWidget extends StatefulWidget {
  const ListsWidget({Key? key}) : super(key: key);

  @override
  _ListsWidgetState createState() => _ListsWidgetState();
}

class _ListsWidgetState extends State<ListsWidget> {
  List<AppList> lists = [];

  @override
  void initState() {
    getLists();
    super.initState();
  }

  void getLists() async {
    final listsJson = await apiGET('/lists');

    setState(() {
      lists = List.from(listsJson['data'])
          .map(
            (e) => AppList.fromJson(e['attributes']),
          )
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: lists.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context, i) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () => Navigator.of(context).pop<AppList>(lists[i]),
            child: Card(
              color: Colors.green[100],
              child: Center(
                child: Text(
                  lists[i].title,
                  style: const TextStyle(fontSize: 24.0),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
