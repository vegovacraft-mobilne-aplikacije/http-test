import 'package:flutter/material.dart';
import 'package:http_test/models/user.model.dart';

class UserTableWidget extends StatefulWidget {
  const UserTableWidget({Key? key}) : super(key: key);

  @override
  _UserTableWidgetState createState() => _UserTableWidgetState();
}

class _UserTableWidgetState extends State<UserTableWidget> {
  List<User> _users = [];

  @override
  void initState() {
    _fetchUsers();
    super.initState();
  }

  void _fetchUsers() async {
    final users = await User.getUsers();
    setState(() => _users = users);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _users.map((e) => Text(e.id.toString())).toList(),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _users.map((e) => Text(e.name)).toList(),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _users.map((e) => Text(e.email)).toList(),
        ),
      ],
    );
  }
}
