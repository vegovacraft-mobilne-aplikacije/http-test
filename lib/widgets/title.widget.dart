import 'package:flutter/material.dart';

const _titleStyle = TextStyle(
  fontSize: 64.0,
  fontWeight: FontWeight.w100,
);

class TitleWidget extends StatelessWidget {
  const TitleWidget(this.title, {Key? key}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: _titleStyle,
    );
  }
}
