import 'package:flutter/material.dart';

class CreateItemPrompt extends StatelessWidget {
  const CreateItemPrompt({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Dialog(
      child: Text('Create Item Prompt'),
    );
  }
}
