import 'package:flutter/material.dart';

class LogoutPrompt extends StatelessWidget {
  const LogoutPrompt({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Logout'),
      content: const Text('Do you really want to logout?'),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop<bool>(false),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop<bool>(true),
          child: const Text('Logout'),
        ),
      ],
    );
  }
}
