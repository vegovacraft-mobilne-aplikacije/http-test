import 'package:flutter/material.dart';
import 'package:http_test/models/user.model.dart';

class UserListWidget extends StatefulWidget {
  const UserListWidget({Key? key}) : super(key: key);

  @override
  _UserListWidgetState createState() => _UserListWidgetState();
}

class _UserListWidgetState extends State<UserListWidget> {
  List<User> _users = [];

  @override
  void initState() {
    _fetchUsers();
    super.initState();
  }

  void _fetchUsers() async {
    final users = await User.getUsers();
    setState(() => _users = users);
  }

  void _onClick(User user) {
    Navigator.of(context).pushNamed(
      '/user',
      arguments: user,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _users.length,
      itemBuilder: (context, i) {
        final user = _users[i];
        return GestureDetector(
          onTap: () => _onClick(user),
          child: Card(
            child: ListTile(
              title: Text(user.name),
            ),
          ),
        );
      },
    );
  }
}
