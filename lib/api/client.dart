import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_test/api/exceptions/bad_request.exception.dart';
import 'package:http_test/api/exceptions/forbidden.exception.dart';
import 'package:shared_preferences/shared_preferences.dart';

const apiUrl = 'https://flutter.aikenahac.com/api';

// const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjQxNTgxMTk1LCJleHAiOjE2NDQxNzMxOTV9.4PBbUWCLaY4PLM4p_Hc_8e2f5SvS7QVdv3-eh5_b5mk';
String? token;

Future<List<Map<String, dynamic>>> makeGET() async {
  final url = Uri.parse('https://jsonplaceholder.typicode.com/users');

  final json = await http.read(url);

  final jsonList = List.from(jsonDecode(json));

  return jsonList.map((e) => Map<String, dynamic>.from(e)).toList();
}

Future<Map<String, dynamic>> apiGET(String endpoint) async {
  final url = Uri.parse(apiUrl + endpoint);

  SharedPreferences storage = await SharedPreferences.getInstance();

  token = storage.getString('token');

  final headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  if (token != null) {
    headers.addAll({
      'Authorization': token ?? ' ',
    });
  }

  final response = await http.get(
    url,
    headers: headers,
  );

  final Map<String, dynamic> responseBody = jsonDecode(response.body);

  return responseBody;
}

Future<Map<String, dynamic>> apiPOST(String endpoint, Map<String, dynamic> body) async {
  final url = Uri.parse(apiUrl + endpoint);

  SharedPreferences storage = await SharedPreferences.getInstance();

  token = storage.getString('token');

  final headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  if (token != null) {
    headers.addAll({
      'Authorization': token ?? '',
    });
  }

  final response = await http.post(
    url,
    headers: headers,
    body: jsonEncode(body),
  );

  final Map<String, dynamic> responseBody = jsonDecode(response.body);

  if (response.statusCode > 299) {
    switch (response.statusCode) {
      case 400:
        throw BadRequestException(responseBody['error']['message']);
      case 403:
        throw ForbiddenException(responseBody['error']['message']);
    }
  }

  return responseBody;
}

Future createList(data) async {
  final url = Uri.parse('http://flutter.aikenahac.com/api/lists');

  SharedPreferences storage = await SharedPreferences.getInstance();

  token = storage.getString('token');

  final response = await http.post(
    url,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token ?? '',
    },
    body: jsonEncode(data),
  );

  final res = Map.from(jsonDecode(response.body));
}

Future getList(id) async {
  final url = Uri.parse('http://flutter.aikenahac.com/api/lists/$id?populate=*');

  SharedPreferences storage = await SharedPreferences.getInstance();

  token = storage.getString('token');

  final response = await http.get(
    url,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token ?? '',
    },
  );

  final res = Map.from(jsonDecode(response.body));

  return res;
}

Future addToList(data, id) async {
  final url = Uri.parse('http://flutter.aikenahac.com/api/lists/$id');

  SharedPreferences storage = await SharedPreferences.getInstance();

  token = storage.getString('token');

  final response = await http.put(
    url,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': token ?? '',
    },
    body: jsonEncode(data),
  );

  final res = Map.from(jsonDecode(response.body));

  print(res);
}
