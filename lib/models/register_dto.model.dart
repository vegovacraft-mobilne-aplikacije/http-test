import 'package:json_annotation/json_annotation.dart';

part 'register_dto.model.g.dart';

@JsonSerializable()
class RegisterDto {
  final String username, email, password;

  RegisterDto({
    required this.username,
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toJson() => _$RegisterDtoToJson(this);
}
