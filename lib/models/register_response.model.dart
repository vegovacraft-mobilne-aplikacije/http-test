import 'package:json_annotation/json_annotation.dart';

part 'register_response.model.g.dart';

@JsonSerializable()
class RegisterResponse {
  final String jwt;
  final dynamic user;

  RegisterResponse(this.jwt, this.user);

  factory RegisterResponse.fromJson(Map<String, dynamic> json) =>
      _$RegisterResponseFromJson(json);
}
