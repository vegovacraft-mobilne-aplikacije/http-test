import 'package:http_test/api/client.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.model.g.dart';

@JsonSerializable()
class User {
  final int id;
  final String name, username, email;

  User(this.id, this.name, this.username, this.email);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static Future<List<User>> getUsers() async {
    final json = await makeGET();
    return json.map((e) => User.fromJson(e)).toList();
  }
}
