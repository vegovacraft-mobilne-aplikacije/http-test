import 'package:json_annotation/json_annotation.dart';

part 'list.model.g.dart';

@JsonSerializable()
class AppList {
  final String title;

  AppList(this.title);

  factory AppList.fromJson(Map<String, dynamic> json) => _$AppListFromJson(json);
}
