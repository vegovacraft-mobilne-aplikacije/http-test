// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppList _$AppListFromJson(Map<String, dynamic> json) => AppList(
      json['title'] as String,
    );

Map<String, dynamic> _$AppListToJson(AppList instance) => <String, dynamic>{
      'title': instance.title,
    };
