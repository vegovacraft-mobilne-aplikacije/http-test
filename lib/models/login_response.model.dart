import 'package:json_annotation/json_annotation.dart';

part 'login_response.model.g.dart';

@JsonSerializable()
class LoginResponse {
  final String jwt;
  final dynamic user;

  LoginResponse(this.jwt, this.user);

  factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);
}
