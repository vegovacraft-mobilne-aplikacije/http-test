import 'package:flutter/material.dart';
import 'package:http_test/widgets/items_list.widget.dart';

class ListScreen extends StatelessWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Items'),
      ),
      body: const ItemListWidget(),
    );
  }
}
