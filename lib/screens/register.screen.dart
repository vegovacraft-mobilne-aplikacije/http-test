import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:http_test/api/client.dart';
import 'package:http_test/api/exceptions/forbidden.exception.dart';
import 'package:http_test/models/register_dto.model.dart';
import 'package:http_test/models/register_response.model.dart';
import 'package:http_test/widgets/title.widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _emailStyle = InputDecoration(
  label: Text('Email'),
  icon: Icon(Icons.email),
);

const _usernameStyle = InputDecoration(
  label: Text('Username'),
  icon: Icon(Icons.person),
);

const _passwordStyle = InputDecoration(
  label: Text('Password'),
  icon: Icon(Icons.vpn_key),
);

const _registerButtonTextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.white,
);

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    final _emailController = TextEditingController();
    final _usernameController = TextEditingController();
    final _passwordController = TextEditingController();

    void _showMessage(String message) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
    }

    bool _checkFields() {
      if (!EmailValidator.validate(_emailController.text)) {
        _showMessage('Please enter a valid email');
        return false;
      }

      if (_usernameController.text.length <= 2) {
        _showMessage('Username has to be longer than 2 characters');
        return false;
      }

      if (_passwordController.text.length <= 5) {
        _showMessage('Password has to be longer than 5 characters');
        return false;
      }

      return true;
    }

    void _onRegister() async {
      if (!_checkFields()) {
        return;
      }

      SharedPreferences storage = await SharedPreferences.getInstance();

      final registerDto = RegisterDto(
        username: _usernameController.text,
        email: _emailController.text,
        password: _passwordController.text,
      );

      try {
        final registerResponseJson = await apiPOST(
          '/auth/local/register',
          registerDto.toJson(),
        );

        final registerResponse = RegisterResponse.fromJson(registerResponseJson);

        final String token = 'Bearer ' + registerResponse.jwt;

        await storage.setString('token', token);

        Navigator.of(context).pushNamed('/');
      } catch (e) {
        print(e);
        if (e is ForbiddenException) {
          _showMessage('This user already exists');
        }
      }
    }

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const TitleWidget('Register'),
            const Spacer(),
            TextField(
              controller: _emailController,
              decoration: _emailStyle,
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _usernameController,
              decoration: _usernameStyle,
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: _passwordStyle,
            ),
            const SizedBox(height: 32.0),
            SizedBox(
              width: width * 0.5,
              height: height * 0.06,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(const Color(0xFFB89BC9)),
                ),
                onPressed: _onRegister,
                child: const Text(
                  'Register',
                  style: _registerButtonTextStyle,
                ),
              ),
            ),
            const Spacer(),
            TextButton(
              onPressed: () => Navigator.of(context).pushReplacementNamed('/login'),
              child: const Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
