import 'package:flutter/material.dart';
import 'package:http_test/api/client.dart';
import 'package:http_test/models/list.model.dart';
import 'package:http_test/widgets/create_item_prompt.widget.dart';
import 'package:http_test/widgets/logout_prompt.widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _name = TextEditingController();

  final TextEditingController _quantity = TextEditingController();

  String loggedIn = 'no';
  AppList? selectedList;

  @override
  void initState() {
    super.initState();
    _isAuthenticated();
  }

  _createNewList() {
    final data = {
      'data': {
        'title': 'Flutter',
        'items': [
          {
            '__component': 'list-items.item',
            'name': 'Komponent',
            'quantity': 1,
          }
        ]
      }
    };

    return data;
  }

  _addToList(name, quantity) {
    final data = {
      'data': {
        'items': [
          {
            '__component': 'list-items.item',
            'name': '$name',
            'quantity': int.parse(quantity),
          }
        ]
      }
    };

    return data;
  }

  _isAuthenticated() async {
    final request = await apiGET('/users/me');

    setState(() {
      if (request['username'] != null) {
        loggedIn = 'yes';
      } else {
        Navigator.of(context).pushReplacementNamed('/start');
      }
    });
  }

  _logOut() async {
    final shouldLogout = await showDialog<bool>(
          context: context,
          builder: (context) => const LogoutPrompt(),
        ) ??
        false;

    if (!shouldLogout) {
      return;
    }

    SharedPreferences storage = await SharedPreferences.getInstance();

    storage.remove('token');

    Navigator.of(context).pushNamed('/');
  }

  void _onSelectList() async {
    final appList = await Navigator.of(context).pushNamed('/lists') as AppList?;
    if (appList == null) {
      return;
    }
    setState(() => selectedList = appList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          onTap: _onSelectList,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(Icons.arrow_drop_down_rounded),
              Text(selectedList?.title ?? 'Select'),
            ],
          ),
        ),
        actions: [
          IconButton(
            onPressed: _logOut,
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog(
          context: context,
          builder: (context) => const CreateItemPrompt(),
        ),
        child: const Icon(Icons.add),
      ),
      body: Center(
        child: Column(
          children: [
            TextButton(
              onPressed: () async => {
                await createList(
                  _createNewList(),
                )
              },
              child: const Text('Create data'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _name,
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _quantity,
                decoration: const InputDecoration(
                  labelText: 'Quantity',
                ),
              ),
            ),
            TextButton(
              onPressed: () async => {
                await addToList(
                  _addToList(_name.text, _quantity.text),
                  10,
                )
              },
              child: const Text('Add item'),
            ),
            TextButton(
              child: const Text('Get data'),
              onPressed: () => Navigator.of(context).pushNamed('/list'),
            ),
            const Spacer(),
            Text(
              'Is logged in: $loggedIn',
              style: const TextStyle(fontSize: 16.0),
            ),
            const SizedBox(height: 32.0),
          ],
        ),
      ),
    );
  }
}
