import 'package:flutter/material.dart';

const _textStyle = TextStyle(
  color: Colors.white,
);

class StartScreen extends StatelessWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Center(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                'assets/logo.png',
                width: 300.0,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: width * 0.8,
                    height: height * 0.06,
                    child: TextButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(const Color(0xFFB89BC9)),
                      ),
                      onPressed: () => Navigator.of(context).pushReplacementNamed('/register'),
                      child: const Text(
                        'Register',
                        style: _textStyle,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  SizedBox(
                    width: width * 0.8,
                    height: height * 0.06,
                    child: TextButton(
                      style: ButtonStyle(
                        side: MaterialStateProperty.all(
                          const BorderSide(
                            color: Color(0xFFB89BC9),
                            width: 2.0,
                          ),
                        ),
                        backgroundColor: MaterialStateProperty.all(Colors.white),
                      ),
                      onPressed: () => Navigator.of(context).pushReplacementNamed('/login'),
                      child: Text(
                        'Login',
                        style: _textStyle.copyWith(color: const Color(0xFFB89BC9)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
