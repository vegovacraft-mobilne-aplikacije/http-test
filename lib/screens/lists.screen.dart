import 'package:flutter/material.dart';
import 'package:http_test/widgets/lists.widget.dart';

class ListsScreen extends StatelessWidget {
  const ListsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pick a List'),
      ),
      body: const ListsWidget(),
    );
  }
}
