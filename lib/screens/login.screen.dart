import 'package:flutter/material.dart';
import 'package:http_test/api/client.dart';
import 'package:http_test/api/exceptions/bad_request.exception.dart';
import 'package:http_test/models/login_dto.model.dart';
import 'package:http_test/models/login_response.model.dart';
import 'package:http_test/widgets/title.widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _usernameStyle = InputDecoration(
  label: Text('Username'),
  icon: Icon(Icons.person),
  border: OutlineInputBorder(),
);

const _passwordStyle = InputDecoration(
  label: Text('Password'),
  icon: Icon(Icons.vpn_key),
  border: OutlineInputBorder(),
);

const _loginButtonTextStyle = TextStyle(
  fontSize: 16.0,
  color: Colors.white,
);

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  final bool _isPasswordErrored = true;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    final _usernameController = TextEditingController();
    final _passwordController = TextEditingController();

    void _showMessage(String message) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
    }

    bool _checkFields() {
      if (_usernameController.text.length <= 2) {
        _showMessage('Username has to be longer than 2 characters');
        return false;
      }

      if (_passwordController.text.length <= 5) {
        _showMessage('Password has to be longer than 5 characters');
        return false;
      }

      return true;
    }

    void _onLogin() async {
      if (!_checkFields()) {
        return;
      }

      SharedPreferences storage = await SharedPreferences.getInstance();

      final loginDto = LoginDto(
        identifier: _usernameController.text,
        password: _passwordController.text,
      );

      try {
        final loginResponseJson = await apiPOST(
          '/auth/local',
          loginDto.toJson(),
        );

        final loginResponse = LoginResponse.fromJson(loginResponseJson);

        token = 'Bearer ' + loginResponse.jwt;

        await storage.setString('token', token!);

        Navigator.of(context).pushReplacementNamed('/');
      } catch (e) {
        if (e is BadRequestException) {
          _showMessage(e.message);
        }
      }
    }

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const TitleWidget('Login'),
            const Spacer(),
            TextField(
              decoration: _usernameStyle,
              controller: _usernameController,
            ),
            const SizedBox(height: 16.0),
            TextField(
              decoration: _passwordStyle,
              obscureText: true,
              controller: _passwordController,
            ),
            const SizedBox(height: 32.0),
            SizedBox(
              width: width * 0.5,
              height: height * 0.06,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(const Color(0xFFB89BC9)),
                ),
                onPressed: _onLogin,
                child: const Text(
                  'Login',
                  style: _loginButtonTextStyle,
                ),
              ),
            ),
            const Spacer(flex: 2),
            TextButton(
              onPressed: () => Navigator.of(context).pushReplacementNamed('/register'),
              child: const Text('Register'),
            ),
          ],
        ),
      ),
    );
  }
}
